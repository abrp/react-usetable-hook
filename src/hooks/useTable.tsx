const useTable = <
  TData,
  TColumns extends {
    [key: string]: string | ((arg: TData) => string)
    label: string
  }
>({
  data,
  columns,
}: {
  data: TData[]
  columns: TColumns[]
}) => {
  const headers = columns.map(({ accessor, label }) => ({ accessor, label }))

  const rows = data.map((dataRow) => {
    const cells = columns.map(({ accessor }) => {
      return {
        renderedValue:
          typeof accessor === "function"
            ? accessor(dataRow)
            : dataRow[accessor as keyof TData],
      }
    })

    return { cells }
  })

  return { headers, rows }
}

export { useTable }
