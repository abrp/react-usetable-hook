import { mockData } from "./mockData"
import { useTable } from "../../hooks/useTable"

import { parse, format, setDefaultOptions } from "date-fns"
import { fr } from "date-fns/locale"
setDefaultOptions({ locale: fr })

import "./Table.css"

export type Data = {
  id: number
  date: string
  name: string
  price: number
}

type Columns = {
  accessor: string | ((arg: Data) => string)
  label: string
}

const COLUMNS: Columns[] = [
  {
    accessor: ({ date }) => formatDate(date),
    label: "Date",
  },
  { accessor: "name", label: "Article" },
  { accessor: ({ price }) => `${price} €`, label: "Prix" },
]

const data: Data[] = mockData

const formatDate = (date: string) => {
  return format(parse(date, "yyyy-MM-dd", new Date()), "EEEE dd MMM yyyy")
}

const { headers, rows } = useTable({ data, columns: COLUMNS })

const Table = () => {
  return (
    <table>
      <thead>
        <tr>
          {COLUMNS.map(({ label }, index) => (
            <th key={index}>{label}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {rows.map(({ cells }, index) => (
          <tr key={index}>
            {cells.map(({ renderedValue }, index) => (
              <td key={index}>{renderedValue}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  )
}

export { Table }
