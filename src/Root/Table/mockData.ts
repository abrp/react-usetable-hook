export const mockData = [
  {
    id: 1,
    date: "2023-01-01",
    name: "Croissant",
    price: 1.0,
  },
  {
    id: 2,
    date: "2023-01-02",
    name: "Gateau au chocolat",
    price: 1.1,
  },
]
