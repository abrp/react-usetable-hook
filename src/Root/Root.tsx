import { Table } from "./Table"

const Root = () => {
  return (
    <div>
      <Table />
    </div>
  )
}

export default Root
